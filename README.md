# Repositorio plantilla: "Compra en tienda"

Para entregar este ejercicio, crea una bifurcación (fork) de este repositorio, y sube a él tu solución. Puedes consultar el [enunciado](https://gitlab.eif.urjc.es/cursoprogram/materiales/-/blob/main/practicas/datos/tienda/README.md).


